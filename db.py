import pymysql
from usuario import estudiante
from usuario import tutor
from usuario import usuario


class DataBase:

    def __init__(self):
        self.connection = pymysql.connect(
            host='localhost',
            user='root',
            password='',
            db='tutorsearch'
        )
        self.cursor = self.connection.cursor()
        print('conexion exitosa')

    def get_usuario(self, id):
        sql = f'select id_usuario , nombre , email, numero from usuario where id_usuario = {id}'

        try:

            self.cursor.execute(sql)

            usuario = self.cursor.fetchone()

            return usuario

        except Exception as e:

            raise

    def create_usuario(self, id_usuario, nombre, email, numero):
        sql = f'insert into usuario (`id_usuario`, `nombre`, `email`, `numero`) VALUES ({id_usuario},\'{nombre}\',\'{email}\',\'{numero}\')'
        try:

            self.cursor.execute(sql)
            self.connection.commit()



        except Exception as e:
            raise

    def get_tutor(self, id_tutor):
        sql = f'select id_tutor , id_usuario , costo_tutoria, cuenta_bancaria from tutor where id_tutor = {id_tutor}'
        try:
            self.cursor.execute(sql)
            tutor = self.cursor.fetchone()

            return tutor


        except Exception as e:
            raise

    def create_tutor(self, id_tutor, id_usuario, costo_tutoria, cuenta_bancaria):
        sql = f'INSERT INTO `tutor` (`id_tutor`,`id_usuario`, `costo_tutoria`,`cuenta_bancaria`) VALUES({id_tutor},{id_usuario},{costo_tutoria},\'{cuenta_bancaria}\')'
        try:
            self.cursor.execute(sql)
            self.connection.commit()

        except Exception as e:
            raise
